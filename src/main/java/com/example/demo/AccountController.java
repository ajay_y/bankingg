package com.example.demo;

import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/account")
@RestController
public class AccountController {
	@Autowired
	UserService userService;
	@GetMapping("/deposit")
	private void deposit(@RequestBody deposit deposit) {
		System.out.println("Amout deposited: "+ deposit.getAmount());
	}
	@GetMapping("/withdrawal")
	private void withdrawal(@RequestBody withdrawal withdrawal) {
		System.out.println("Remaining balance :"+ withdrawal.getbalance());
	}
//	@PostMapping("/showAccount")
//	private void showAccount(@RequestBody showAccount showAccount) {
//		System.out.println("Account Details: "+ showAccount.getdetails());
//	}
	@PostMapping
	private String openAccount(@RequestBody openAccount openaccount) {
		userService.save(openaccount);
		System.out.println("Account holder name: " + openaccount.getName());
		return "post called";
	}
}
